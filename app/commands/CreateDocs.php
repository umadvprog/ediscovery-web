<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use Phrocco\PhroccoGroup;
use Phrocco\Phrocco;

class CreateDocs extends Command {

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'generate:docs';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate documents in view/docs.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $language = $this->argument('language');
        $files = $this->argument('files');
        $folder = $this->argument('folder');

        $output = app_path() . '/views/docs/'.$folder;


        if($language == "php" && $files)
        {
            if($this->str_ends_with($files, ".php"))
            {
                $endPos = strripos($files, "/");
                $fileName = substr($files, $endPos);
                $fileName = rtrim($fileName, '.php').".blade.php";


                $output .= $fileName;

                $single = new Phrocco($language, $files, array("template" => app_path()."/views/docs/template.blade.php"), $output);
                $single->write($single->render());
            }
            else
            {
                $group = new PhroccoGroup(["i"=>$files, "o"=>$output], $folder);
                $group->process();
                $group->write();
            }
        }

    }

    private function str_ends_with($haystack, $needle)
    {
        return strpos($haystack, $needle) + strlen($needle) === strlen($haystack);
    }


    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {

        return array(
            array('language', InputArgument::REQUIRED, 'Specify what language is being documented'),
            array('folder', InputArgument::REQUIRED, 'Specify what folder in views/docs you want this go'),
            array('files', InputArgument::REQUIRED, 'Specify what files you want to document with a fully qualified path')
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
       return array();
    }

}
