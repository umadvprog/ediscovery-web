@extends('docs.master')

@include('docs._partials.navigation')

@include('docs._partials.footer')

@section('content')

<div id="container">
    <div id="background"></div>
                    <div id="jump_to">
            Jump To &hellip;
            <div id="jump_wrapper">
                <div id="jump_page">
                    <ul>
                                                                                                                        <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/api/DocumentAPI">
                                                                        DocumentAPI.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/api/CategoryAPI">
                                                                        CategoryAPI.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/api/EItemAPI">
                                                                        EItemAPI.php                                    </a></li>
                                                    </ul>
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th class="docs">
                <h1>
                    EItemAPI.php                </h1>
            </th>
            <th class="code">
            </th>
        </tr>
        </thead>
        <tbody>
                    <tr id="section-0">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-0">&#182;</a>
                    </div>
                    <p>===================================================================================================
 EItemAPI.php</p>

<hr />

<p>eDiscovery Web Application</p>

<p><a href="https://bitbucket.org/umadvprog">Advanced Programming Team</a></p>

<p><em class='docparam'>@authors</em> Angela Gross, Scott Halstvedt</p>

<p>Allows user to view all EItems or just some information about a particular EItem.
 ===================================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="k">class</span> <span class="nc">EItemAPI</span> <span class="k">extends</span> <span class="nx">BaseController</span> 
<span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-1">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-1">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-2">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-2">&#182;</a>
                    </div>
                    <p>======================================================================================</p>

<h3>eItems()</h3>

<hr />

<p>If a single EItem is selected, then it grabs all associated categories along with the
 file. If all EItems are selected (no ID is given) then just grabs file information.</p>

<p><em class='docparam'>@param</em> <code>int $eItemID</code>
<em class='docparam'>@return</em> <code>JSON</code>
 ======================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>  <span class="k">public</span> <span class="k">function</span> <span class="nf">eItems</span><span class="p">(</span><span class="nv">$eItemID</span> <span class="o">=</span> <span class="k">null</span><span class="p">)</span>
  <span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-3">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-3">&#182;</a>
                    </div>
                    <p>If no eItem ID is specified, then grab all eItems</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>   <span class="k">if</span> <span class="p">(</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$eItemID</span><span class="p">))</span> 
   <span class="p">{</span>
                <span class="nv">$eItems</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span></pre></div>                </td>
            </tr>
                    <tr id="section-4">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-4">&#182;</a>
                    </div>
                    <p>Format each EItem with documents and ID</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                <span class="k">foreach</span><span class="p">(</span><span class="nx">EItem</span><span class="o">::</span><span class="na">all</span><span class="p">()</span> <span class="k">as</span> <span class="nv">$eItem</span><span class="p">)</span>
                <span class="p">{</span>
                    <span class="nv">$document</span> <span class="o">=</span> <span class="nv">$eItem</span><span class="o">-&gt;</span><span class="na">Document</span><span class="p">;</span>
                    <span class="nv">$file</span> <span class="o">=</span> <span class="k">null</span><span class="p">;</span></pre></div>                </td>
            </tr>
                    <tr id="section-5">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-5">&#182;</a>
                    </div>
                    <p>Does it have a document? If not, then try an email</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                    <span class="k">if</span><span class="p">(</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$document</span><span class="p">))</span>
                    <span class="p">{</span>
                        <span class="nv">$email</span> <span class="o">=</span> <span class="nv">$eItem</span><span class="o">-&gt;</span><span class="na">Email</span><span class="p">;</span></pre></div>                </td>
            </tr>
                    <tr id="section-6">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-6">&#182;</a>
                    </div>
                    <p>Does it have an email? If so, grab the file</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                        <span class="k">if</span><span class="p">(</span><span class="o">!</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$email</span><span class="p">))</span>
                        <span class="p">{</span>
                            <span class="nv">$file</span> <span class="o">=</span> <span class="nv">$email</span><span class="o">-&gt;</span><span class="na">FileTable</span><span class="p">;</span>
                        <span class="p">}</span>
                    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-7">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-7">&#182;</a>
                    </div>
                    <p>If it does have a document, grab the file</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                    <span class="k">else</span>
                    <span class="p">{</span>
                        <span class="nv">$file</span> <span class="o">=</span> <span class="nv">$document</span><span class="o">-&gt;</span><span class="na">FileTable</span><span class="p">;</span>
                    <span class="p">}</span>

                    <span class="nv">$eItems</span><span class="p">[]</span> <span class="o">=</span> <span class="k">array</span>
                    <span class="p">(</span>
                        <span class="s1">&#39;E-item ID&#39;</span> <span class="o">=&gt;</span> <span class="nv">$eItem</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;E-item ID&#39;</span><span class="p">},</span>
                        <span class="s1">&#39;File&#39;</span> <span class="o">=&gt;</span> <span class="nv">$file</span><span class="o">-&gt;</span><span class="na">toArray</span><span class="p">()</span>
                    <span class="p">);</span>
                <span class="p">}</span>

    <span class="k">return</span> <span class="nv">$eItems</span><span class="p">;</span>
   <span class="p">}</span> 
   <span class="k">else</span> 
   <span class="p">{</span>
    <span class="nv">$eItem</span> <span class="o">=</span> <span class="nx">EItem</span><span class="o">::</span><span class="na">find</span><span class="p">(</span><span class="nv">$eItemID</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-8">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-8">&#182;</a>
                    </div>
                    <p>If eItem not found, tell user.</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">if</span> <span class="p">(</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$eItem</span><span class="p">))</span>
    <span class="p">{</span>
     <span class="k">return</span> <span class="nx">Response</span><span class="o">::</span><span class="na">json</span><span class="p">(</span><span class="s1">&#39;E-Item not found&#39;</span><span class="p">,</span> <span class="mi">404</span><span class="p">);</span>
    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-9">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-9">&#182;</a>
                    </div>
                    <p>Else, format eItem and return it</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">else</span> 
    <span class="p">{</span>
                    <span class="nv">$document</span> <span class="o">=</span> <span class="nv">$eItem</span><span class="o">-&gt;</span><span class="na">Document</span><span class="p">;</span>
                    <span class="nv">$file</span> <span class="o">=</span> <span class="k">null</span><span class="p">;</span>
                    <span class="nv">$categories</span> <span class="o">=</span> <span class="nv">$eItem</span><span class="o">-&gt;</span><span class="na">Categories</span><span class="p">();</span></pre></div>                </td>
            </tr>
                    <tr id="section-10">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-10">&#182;</a>
                    </div>
                    <p>Does it have a document? If not, then try an email</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                    <span class="k">if</span><span class="p">(</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$document</span><span class="p">))</span>
                    <span class="p">{</span>
                        <span class="nv">$email</span> <span class="o">=</span> <span class="nv">$eItem</span><span class="o">-&gt;</span><span class="na">Email</span><span class="p">;</span></pre></div>                </td>
            </tr>
                    <tr id="section-11">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-11">&#182;</a>
                    </div>
                    <p>Does it have an email? If so, grab the file</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                        <span class="k">if</span><span class="p">(</span><span class="o">!</span><span class="nb">is_null</span><span class="p">(</span><span class="nv">$email</span><span class="p">))</span>
                        <span class="p">{</span>
                            <span class="nv">$file</span> <span class="o">=</span> <span class="nv">$email</span><span class="o">-&gt;</span><span class="na">FileTable</span><span class="p">;</span>
                        <span class="p">}</span>
                    <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-12">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-12">&#182;</a>
                    </div>
                    <p>If it does have a document, grab the file</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                    <span class="k">else</span>
                    <span class="p">{</span>
                        <span class="nv">$file</span> <span class="o">=</span> <span class="nv">$document</span><span class="o">-&gt;</span><span class="na">FileTable</span><span class="p">;</span>
                    <span class="p">}</span>

                    <span class="nv">$allCategories</span> <span class="o">=</span> <span class="k">array</span><span class="p">();</span></pre></div>                </td>
            </tr>
                    <tr id="section-13">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-13">&#182;</a>
                    </div>
                    <p>Format categories</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>                    <span class="k">foreach</span><span class="p">(</span><span class="nv">$categories</span> <span class="k">as</span> <span class="nv">$category</span><span class="p">)</span>
                    <span class="p">{</span>
                        <span class="nv">$category</span> <span class="o">=</span> <span class="nx">Category</span><span class="o">::</span><span class="na">find</span><span class="p">(</span><span class="nv">$category</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;Category ID&#39;</span><span class="p">});</span>
                        <span class="nv">$allCategories</span><span class="p">[]</span> <span class="o">=</span> <span class="k">array</span>
                        <span class="p">(</span>
                            <span class="s1">&#39;Category ID&#39;</span> <span class="o">=&gt;</span> <span class="nv">$category</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;Category ID&#39;</span><span class="p">},</span>
                            <span class="s1">&#39;Category String&#39;</span> <span class="o">=&gt;</span> <span class="nv">$category</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;Category String&#39;</span><span class="p">}</span>
                        <span class="p">);</span>
                    <span class="p">}</span>

                    <span class="nv">$formatEItem</span> <span class="o">=</span> <span class="k">array</span>
                    <span class="p">(</span>
                        <span class="s1">&#39;E-item ID&#39;</span> <span class="o">=&gt;</span> <span class="nv">$eItem</span><span class="o">-&gt;</span><span class="p">{</span><span class="s1">&#39;E-item ID&#39;</span><span class="p">},</span>
                        <span class="s1">&#39;File&#39;</span> <span class="o">=&gt;</span> <span class="nv">$file</span><span class="o">-&gt;</span><span class="na">toArray</span><span class="p">(),</span>
                        <span class="s1">&#39;Categories&#39;</span> <span class="o">=&gt;</span> <span class="nv">$allCategories</span>
                    <span class="p">);</span>

     <span class="k">return</span> <span class="nv">$formatEItem</span><span class="p">;</span>
    <span class="p">}</span>
   <span class="p">}</span>
  <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-14">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-14">&#182;</a>
                    </div>
                    <p>////////////////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="p">}</span>
</pre></div></pre></div>                </td>
            </tr>
                </tbody>
    </table>
</div>

@stop