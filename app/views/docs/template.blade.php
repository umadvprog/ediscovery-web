@extends('docs.master')

@include('docs._partials.navigation')

@include('docs._partials.footer')

@section('content')

<div id="container">
    <div id="background"></div>
    <?php if(isset($sources)):?>
        <?$folder = false?>
        <div id="jump_to">
            Jump To &hellip;
            <div id="jump_wrapper">
                <div id="jump_page">
                    <ul>
                        <?php foreach($sources as $source):?>
                        <?$name = strtr($source['url'], array(".blade.php" => ""));?>
                        <?php $folder = $source['doc-folder'] ? $source['doc-folder'].'/' : ''; ?>
                        <?$url =  URL::to('docs/'.$folder.$name); ?>
                        <?if(!$folder || $folder !== $source["folder"]):?><li><?=$folder = $source["folder"]?></li><?endif?>
                        <?php if(substr_count($path, "/")===0):?>
                        <li><a class="source level_<?=$source["level"]?>" href="<?=$url?>">
                                <?php else:?>
                                <li><a class="source level_<?=$source["level"]?>" href="<?=$url?>">
                                        <?php endif?>
                                        <?=$source["name"]?>
                                    </a></li>
                                <?php endforeach?>
                    </ul>
                </div>
            </div>
        </div>
    <?php endif?>
    <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th class="docs">
                <h1>
                    <?=$title?>
                </h1>
            </th>
            <th class="code">
            </th>
        </tr>
        </thead>
        <tbody>
        <?php foreach((array)$sections["docs"] as $count=>$section):?>
            <tr id="section-<?=$count?>">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-<?=$count?>">&#182;</a>
                    </div>
                    <?=$sections["docs"][$count]?>
                </td>
                <td class="code">
                    <?=$sections["code"][$count]?>
                </td>
            </tr>
        <?php endforeach?>
        </tbody>
    </table>
</div>

@stop