@extends('docs.master')

@include('docs._partials.navigation')

@include('docs._partials.footer')

@section('content')

<div id="container">
    <div id="background"></div>
                    <div id="jump_to">
            Jump To &hellip;
            <div id="jump_wrapper">
                <div id="jump_page">
                    <ul>
                                                                                                                        <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/models/Category">
                                                                        Category.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/models/Document">
                                                                        Document.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/models/CategoryEItem">
                                                                        CategoryEItem.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/models/EmailItem">
                                                                        EmailItem.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/models/FileTable">
                                                                        FileTable.php                                    </a></li>
                                                                                                                                <li></li>                                                <li><a class="source level_0" href="http://192.81.129.120:8080/docs/models/EItem">
                                                                        EItem.php                                    </a></li>
                                                    </ul>
                </div>
            </div>
        </div>
        <table cellpadding="0" cellspacing="0">
        <thead>
        <tr>
            <th class="docs">
                <h1>
                    Document.php                </h1>
            </th>
            <th class="code">
            </th>
        </tr>
        </thead>
        <tbody>
                    <tr id="section-0">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-0">&#182;</a>
                    </div>
                    <p>===================================================================================================
 Document.php</p>

<hr />

<p>eDiscovery Web Application</p>

<p><a href="https://bitbucket.org/umadvprog">Advanced Programming Team</a></p>

<p><em class='docparam'>@author</em> Angela Gross</p>

<p>Model for the Document table. Contains basic relationship and table information, such as
 getting a Document's associated FileTables and EItems.
 ===================================================================================================</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="k">class</span> <span class="nc">Document</span> <span class="k">extends</span> <span class="nx">Eloquent</span>
<span class="p">{</span></pre></div>                </td>
            </tr>
                    <tr id="section-1">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-1">&#182;</a>
                    </div>
                    <p>///////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-2">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-2">&#182;</a>
                    </div>
                    <h3>TABLE used by model</h3>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-3">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-3">&#182;</a>
                    </div>
                    <p><em class='docparam'>@var</em> <code>string</code></p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">protected</span> <span class="nv">$table</span> <span class="o">=</span> <span class="s1">&#39;EitemData.Document&#39;</span><span class="p">;</span></pre></div>                </td>
            </tr>
                    <tr id="section-4">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-4">&#182;</a>
                    </div>
                    <h3>PRIMARY KEY override</h3>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-5">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-5">&#182;</a>
                    </div>
                    <p><em class='docparam'>@var</em> <code>string</code></p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">protected</span> <span class="nv">$primaryKey</span> <span class="o">=</span> <span class="s1">&#39;Document ID&#39;</span><span class="p">;</span>

    <span class="k">public</span> <span class="nv">$hidden</span> <span class="o">=</span> <span class="k">array</span><span class="p">(</span><span class="s1">&#39;File ID&#39;</span><span class="p">,</span> <span class="s1">&#39;E-item ID&#39;</span><span class="p">);</span></pre></div>                </td>
            </tr>
                    <tr id="section-6">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-6">&#182;</a>
                    </div>
                    <p>///////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-7">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-7">&#182;</a>
                    </div>
                    <h3>RELATIONS</h3>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-8">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-8">&#182;</a>
                    </div>
                    <p>Retrieves a Document's associated FileTables</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-9">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-9">&#182;</a>
                    </div>
                    <p><em class='docparam'>@return</em> mixed</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">public</span> <span class="k">function</span> <span class="nf">FileTable</span><span class="p">()</span> <span class="p">{</span> <span class="k">return</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">belongsTo</span><span class="p">(</span><span class="s1">&#39;App\Models\FileTable&#39;</span><span class="p">,</span> <span class="s1">&#39;File ID&#39;</span><span class="p">);</span> <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-10">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-10">&#182;</a>
                    </div>
                    <p>Retrieves a Document's associated EItems</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre></pre></div>                </td>
            </tr>
                    <tr id="section-11">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-11">&#182;</a>
                    </div>
                    <p><em class='docparam'>@return</em> mixed</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre>    <span class="k">public</span> <span class="k">function</span> <span class="nf">EItem</span><span class="p">()</span> <span class="p">{</span> <span class="k">return</span> <span class="nv">$this</span><span class="o">-&gt;</span><span class="na">belongsTo</span><span class="p">(</span><span class="s1">&#39;App\Models\EItem&#39;</span><span class="p">,</span> <span class="s1">&#39;E-item ID&#39;</span><span class="p">);</span> <span class="p">}</span></pre></div>                </td>
            </tr>
                    <tr id="section-12">
                <td class="docs">
                    <div class="pilwrap">
                        <a class="pilcrow" href="#section-12">&#182;</a>
                    </div>
                    <p>///////////////////////////////////////////////////////////////////////////////////////</p>
                </td>
                <td class="code">
                    <div class="highlight"><pre><span class="p">}</span>
</pre></div></pre></div>                </td>
            </tr>
                </tbody>
    </table>
</div>

@stop