<!DOCTYPE html>
<html>
<head>

    <meta http-equiv="content-type" content="text/html; charset=UTF-8">

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

    <!-- Optional theme -->
    <link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap-theme.min.css">

    <!-- jQuery 1.11.0 -->
    <script src="https://code.jquery.com/jquery-1.11.0.min.js"></script>

    <!-- Latest compiled and minified JavaScript -->
    <script src="//netdna.bootstrapcdn.com/bootstrap/3.1.1/js/bootstrap.min.js"></script>

    {{ HTML::style("css/docs.css") }}
    {{ HTML::style("css/docco.css") }}


    <title><?php echo isset($title) ? $title : "eDiscovery Documentation"; ?></title>


</head>
<body>

@yield('navigation')

<div class="container-fluid">

    @yield('content')

</div>

@yield('footer')

</body>
</html>
