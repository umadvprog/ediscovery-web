<?php namespace App\Models;

use Eloquent, DB;
use App\Models\EmailItem;
use App\Models\Document;
/* ===================================================================================================
// FileTable.php
// ---------------------------------------------------------------------------------------------------
// eDiscovery Web Application
//
// [Advanced Programming Team](https://bitbucket.org/umadvprog)
//
// @author Angela Gross
//
// Model for the FileTable table. Contains basic relationship and table information, such as
// getting an FileTable's associated Emails and Documents.
// ===================================================================================================*/

class FileTable extends Eloquent
{
    /////////////////////////////////////////////////////////////////////////////////////////

    //### TABLE used by model
    // @var `string`
    protected $table = 'ProjectData.FileTable';

    //### PRIMARY KEY override
    // @var `string`
    protected $primaryKey = 'File ID';

    /////////////////////////////////////////////////////////////////////////////////////////

    //### RELATIONS

    // Retrieves a FileTable's associated Emails
    // @return mixed
    public function Emails() { return DB::table((new EmailItem())->getTable())->where('File ID', '=', $this->{'File ID'})->get(); }

    // Retrieves a FileTable's associated Emails
    // @return mixed
    public function Documents() { return DB::table((new Document())->getTable())->where('File ID', '=', $this->{'File ID'})->get(); }

    /////////////////////////////////////////////////////////////////////////////////////////
}