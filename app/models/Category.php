<?php namespace App\Models;

use Eloquent;

/* ===================================================================================================
// Category.php
// ---------------------------------------------------------------------------------------------------
// eDiscovery Web Application
//
// [Advanced Programming Team](https://bitbucket.org/umadvprog)
//
// @author Angela Gross
//
// Model for the Category table. Contains basic relationship and table information, such as getting
// a Category's associated join tables for EItems.
// ===================================================================================================*/

class Category extends Eloquent
{
    /////////////////////////////////////////////////////////////////////////////////////////

    //### TABLE used by model
    // @var `string`
    protected $table = 'CategoryData.Category';

    //### PRIMARY KEY override
    // @var `string`
    protected $primaryKey = 'Category ID';

    /////////////////////////////////////////////////////////////////////////////////////////

    //### RELATIONS

    // Retrieves the join tables between Category and EItem.
    // @return mixed
    public function CategoryEItem() { return $this->belongsTo('App\Models\CategoryEItem', 'Category ID'); }

    /////////////////////////////////////////////////////////////////////////////////////////
}