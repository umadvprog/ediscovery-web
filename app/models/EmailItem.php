<?php namespace App\Models;

use Eloquent;
/* ===================================================================================================
// EmailItem.php
// ---------------------------------------------------------------------------------------------------
// eDiscovery Web Application
//
// [Advanced Programming Team](https://bitbucket.org/umadvprog)
//
// @author Angela Gross
//
// Model for the Email table. Contains basic relationship and table information, such as
// getting an Email's associated EItem and FileTable.
// ===================================================================================================*/

class EmailItem extends Eloquent
{
    /////////////////////////////////////////////////////////////////////////////////////////

    //### TABLE used by model
    // @var `string`
    protected $table = 'EitemData.Email Item';

    //### PRIMARY KEY override
    // @var `string`
    protected $primaryKey = 'Email ID';

    /////////////////////////////////////////////////////////////////////////////////////////

    //### RELATIONS

    // Retrieves a Email's associated FileTable
    // @return mixed
    public function FileTable() { return $this->belongsTo('App\Models\FileTable', 'File ID'); }

    // Retrieves a Email's associated EItem
    // @return mixed
    public function EItem() { return $this->belongsTo('App\Models\EItem', 'E-item ID'); }

    /////////////////////////////////////////////////////////////////////////////////////////
}