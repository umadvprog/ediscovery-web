// Author: Scott Halstvedt

scroll_enabled = false; // enable scroll back to position in list when mouseover resumes

$(document).ready(function() { 
        // this is only necessary for beginning with selected nodes, i.e. if linked to
    $(":checkbox").each(function(i, box) {
        if(box.checked) {
            var color = $(box).data('color');
            var num = parseInt(color.substr(1),16);
            var R = (num >> 16);
            var G = (num >> 8 & 0x00FF);
            var B = (num & 0x0000FF);
            var new_R = (R + 0x55)/2;
            var new_G = (G + 0x55)/2;
            var new_B = (B + 0x55)/2;
            var darker_color = "#" + Math.ceil(new_R).toString(16) + Math.ceil(new_G).toString(16) + Math.ceil(new_B).toString(16);
        
            $(box).css('border-color', color);
            $(box).css('background-color', color);
            $(box).parent().addClass("enabled");
        }
    });
    var shown = true;
    var scroll_pos = 0;
    $('.menu_wrapper>div').mouseenter(function(event) { // handles showing the sidebar on mouseover
            if(!shown) {
                shown = true;
                $(".wrapper").removeClass("disabled_whole");
                if(scroll_enabled) $(".wrapper").animate({scrollTop:scroll_pos}, '100');
                setTimeout(function() { // setTimeout to enable flexbox after layout has settled
                        $(".menu_wrapper").addClass("menu_wrapper_flex");
                    }, 800);
            }
        });
    $('.menu_wrapper>div').mouseleave(function(event) { // handles hiding the sidebar on mouseleave
            if (shown && !$('.search_field').is(':focus') && $('input:checked').length > 0) { // delete flex                                                                                                                                  
                shown = false;
                if(scroll_enabled) {
                    scroll_pos = $(".wrapper").scrollTop(); // do for each                                                                                                                                                                    
                }
                $(".wrapper").animate({scrollTop:0}, '100');
                setTimeout(function() { // setTimeout to allow the layout to settle before disabling flexbox
                        $(".menu_wrapper").removeClass("menu_wrapper_flex");
                        setTimeout(function() {
                                $(".wrapper").addClass("disabled_whole");
                            }, 100);
                    }, 120);
            }
        });
    $('.search_field').bind("enterKey",function(e){
            window.search_term = $(this).val(); // sets the search term when the enter key is pressed
        });
    $('.search_field').keyup(function(e){
            if(e.keyCode == 13) // 13 = "Enter"
                {
                    e.preventDefault(); // prevents the normal behavior on enter on a form input
                    $(this).trigger("enterKey"); // triggers the search term update
                }
        });
    $('form').submit(function() { // intercepts the submission of a form (i.e., enter pressed) to call the search term update and category re-fetch
            window.search_term = $('.search_field').val();
            Categories.fetch({reset: true});
            return false; // return false to not refresh the page on submission
        });
});